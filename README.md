# CV-Cao Jiahang
Mobile: +86-13823087996/ E-mail:caojh7@mail2.sysu.edu.cn

## EDUCATION
### Sun Yat-sen University 09/2018-Present
School of Mathematics
- Degree: Bachelor of Science in Information and Computing Science (expected in 06/2022)
- GPA: 3.7/4.0
- Awarded SYSU Scholarship for Academic Excellence Student for three times (2019 & 2020 & 2021)

## RESEARCH & COMPETITION
### SYSU Turing Lab 04/2021-Present
- Researched literature of Network Compression, Knowledge Graph Learning, Graph Embedding and Gaussian Process
### Research on the Convolution Neural Network based on the Geometric Description 06/2020-11/2020
- Reprocessed images for more image features acquisition through the image’s geometric characteristics extraction 
combined with Convolution Neural Network (CNN)
- Independently completed the images’ geometric characteristics extraction and the deep learning for the images’ feature 
to enhance the interpretability of CNN; the new CNN had a higher accuracy (97%) and an increased rate of convergence
- Finalized the report writing based on the research outcome
### International Genetically Engineered Machine Competition (iGEM) 05/2020-11/2020
- Established the GeneNet model with tensorflow framework and the gene set pathway with gradient descent; acquired the 
gene set pathways (i.e. French Flag, Oscillator and Toggle Switch) with random normal distribution input
- Performed Matlab to solve the differential equations amid gene transcription and visualized the research outcome
### A Traditional Chinese Medicine Recommender System based on AI Technology 05/2020-11/2020
- Initiated the project and led the team to develop a traditional Chinese medicine recommender system comprehending the 
NLP dataset, database concepts, etc. for users’ self-diagnosis and recommended prescription
- The project was registered and authorized as an Innovation and Entrepreneurial Project of SYSU
### A Numerical Simulation of Explosive Clouds Diffusion and Sedimentation 07/2020-10/2020
- Performed the mathematical analysis and data modeling using Hysplit (a meteorological modeling software) and 
predicted the impacts of explosive chemical contaminant in the Pearl River Delta in four seasons
- Completed a project report in accordance; the project was registered and authorized as an Excellent Mathematical & 
Cross-disciplinary Innovative Training Program of the SYSU School of Mathematics (Zhuhai)
### Deep Learning for Stock Selection Based on High Frequency Price-Volume Data 07/2019-09/2019
- Conducted deep learning for the high frequency price data to predict the expected return rate of stock and to select the 
optimized strategies for stock trading
- Developed the Convolution Neural Network and Long Short-Term Memory to fulfil the research purpose and attained a 
yield of 2-5% (with commission)/ yield of 15% (without commission) compared to the broader market
- Finalized the English report accordingly and issued on arXiv.org (https://arxiv.org/abs/1911.02502) 

## INTERNSHIP
### Guangzhou Shining Midas Investment Management Co., Ltd. 07/2019-09/2019
Technical Analyst
- Performed data analysis of acquired market price and determined the key factors, predicted the stock market trend using 
CNN, and completed the backtesting in accordance to verify the selected investment strategies

## COMPETITION AWARD
- Gold Award of International Genetically Engineered Machine Competition (iGEM)
- Third Prize of China Undergraduate Mathematical Contest in Modeling for twice (2019 & 2020)
- Successful Participation of 2020 Mathematical Contest In Modeling (MCM)
- Excellent Prize of National Undergraduate Environmental Protection Knowledge Contest for twice (2019 & 2020) 
## LANGUAGE & SKILL
Language: IELTS (6.5), Chinese (Native)
Skills: Python, Matlab, C++, Golang, C
